from __future__ import print_function

import contextlib
import time
import os

from fabric.context_managers import settings

try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO

from fabric.api import hide, get
from fabric.contrib.files import exists
from fabric.operations import run, put

from utils.log import log
from utils.errors import RunningLocked
from utils.system import tempdir


def can_hide_running():
    def wrapper(function):
        def wrapped(*args, **kwargs):
            hide_running = kwargs.pop('hide_running', False)
            if hide_running:
                with hide('running'):
                    return function(*args, **kwargs)
            else:
                return function(*args, **kwargs)

        return wrapped

    return wrapper


@can_hide_running()
def run_remote(cmd, hide_running=False):
    log.debug("run remote: {}".format(cmd))
    out = StringIO()
    run(cmd, stdout=out)
    return out.getvalue()


@can_hide_running()
def upload_file(local, remote, hide_running=True):
    if not os.path.exists(local):
        raise OSError(local)

    log.debug("upload {} {}".format(local, remote))
    put(local, remote)


@can_hide_running()
def upload_dir(local, remote, hide_running=True):
    upload_file(local, remote, hide_running)


def is_remote_path_exists(path):
    return exists(path)


@can_hide_running()
def read_file(path, hide_running=True):
    tmp_file = StringIO()
    get(path, tmp_file)
    return tmp_file.getvalue()


@can_hide_running()
def remove_file(path, hide_running=True):
    cmd = "rm '{path}'".format(path=path)
    return run_remote(cmd, hide_running=hide_running)


@can_hide_running()
def ensure_remote_path_exists(path, hide_running=True):
    if not is_remote_path_exists(path):
        cmd = "mkdir -p '{path}'".format(path=path)
        return run_remote(cmd, hide_running=hide_running)


@can_hide_running()
def write_to_file(file, data, hide_running=True):
    tmp_file = StringIO()
    tmp_file.write(data)
    put(tmp_file, file)


@contextlib.contextmanager
def wait_remote_lockfile(path, max_duration=600, step=10):
    for waited in range(0, max_duration, step):
        if not is_remote_path_exists(path):
            yield
            return
        log.info("waiting lockfile '{}': {} sec".format(path, waited))
        time.sleep(step)
    raise RunningLocked(path)


def find_remote_files(path):
    with hide('everything'), settings(warn_only=True):
        with tempdir() as tmp_dir:
            ret = get(remote_path=path, local_path=tmp_dir)
            return ret


@contextlib.contextmanager
def wait_remote_lockfiles_in_path(path, max_duration=600, step=10):
    for waited in range(0, max_duration, step):
        files = find_remote_files(path)
        if not files:
            yield
            return
        log.info("waiting lockfiles {files} in path {path}: {time} sec".format(
            files=', '.join(files), path=path, time=waited))
        for fname in files:
            if os.path.exists(fname):
                os.remove(fname)
        time.sleep(step)
    raise RunningLocked(path)
