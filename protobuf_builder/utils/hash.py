from __future__ import print_function
import hashlib
import os
import stat


def md5_for_data(data):
    return hashlib.md5(data).hexdigest()


def md5_for_file(fname):
    # return hashlib.md5(open(fname, 'rb').read()).hexdigest()
    """ memory effective implementation """
    md5 = hashlib.md5()
    with open(fname, 'rb') as f:
        for chunk in iter(lambda: f.read(8192), b''):
            md5.update(chunk)
    return md5.hexdigest()


def _md5_archive_algorithm(data, fname):
    file_md5 = hashlib.md5(data).hexdigest()
    fname_md5 = hashlib.md5(fname).hexdigest()
    # print('=' * 50)
    # print(fname)
    # print("hash({fname}): {hash}".format(fname=fname, hash=file_md5))
    # print("hash({fname}_data): {hash}".format(fname=fname,
    #                                         hash=fname_md5))
    return file_md5 + fname_md5


def md5_as_for_archive(root, files, archive_name):
    hashes = []
    for fname in sorted(files):
        full_path = os.path.join(root, fname)
        with open(full_path) as f:
            data = f.read()
            hashes.append(_md5_archive_algorithm(data, fname))

    result_hash = hashlib.md5("".join(hashes)).hexdigest()
    # print("#" * 100)
    # print("#  path: {path} {hash}".format(path=archive_name, hash=result_hash))
    # print("#" * 100)
    return result_hash


def traverse_hash(h, path, ignore={'.DS_Store', 'Thumbs.db'}):
    rs = os.lstat(path)
    quoted_name = repr(path)
    if stat.S_ISDIR(rs.st_mode):
        h.update('dir ' + quoted_name + '\n')
        for entry in sorted(os.listdir(path)):
            traverse_hash(h, os.path.join(path, entry))
    elif stat.S_ISREG(rs.st_mode):
        ph, name = os.path.split(path)
        if name in ignore:
            return
        h.update('reg ' + quoted_name + ' ')
        h.update(str(rs.st_size) + ' ')
        h.update(md5_for_file(path) + '\n')
    else:
        pass  # ignore symlinks and other special files


def hash_for_path(path):
    if not os.path.exists(path):
        raise AttributeError("path {path} doesn't exists".format(path=path))

    if os.path.isfile(path):
        return md5_for_file(path)

    h = hashlib.sha256()
    traverse_hash(h, path)
    h.update('end\n')
    return h.hexdigest()
