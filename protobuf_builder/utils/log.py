import logging
import os
import sys
import traceback

import colorlog

from utils import suppress

log = colorlog.getLogger(__package__)
logging.basicConfig()

DELIMETER = '_' * 80


def log_except_hook(*exc_info):
    text = "".join(traceback.format_exception(*exc_info))
    logging.error("Unhandled exception: %s", text)


sys.excepthook = log_except_hook


def _set_basic_config(log_level, formatter, **kwargs):
    """Call ``logging.basicConfig`` and override the formatter it creates."""
    logging.basicConfig(level=log_level, **kwargs)
    logging._acquireLock()
    try:
        stream = logging.root.handlers[0]
        stream.setFormatter(formatter)
        stream.setLevel(log_level)
    finally:
        logging._releaseLock()


def get_format(color=False, verbose=False):
    fmt = '%(levelname)-8s %(message)s'

    if verbose:
        fmt = '%(asctime)s ' + fmt + ' [in %(pathname)s:%(lineno)d]%(reset)s'

    if color:
        fmt = '%(log_color)s ' + fmt

    return fmt


def get_formatter(color=True, verbose=False):
    fmt = get_format(color, verbose)

    if color:
        formatter = colorlog.ColoredFormatter(
            fmt,
            datefmt=None,
            reset=True,
            log_colors={
                'DEBUG': 'green',
                'VERBOSE': 'cyan',
                'WARNING': 'yellow',
                'ERROR': 'red',
                'CRITICAL': 'bold_red',
            },
            secondary_log_colors={},
            style='%')
    else:
        formatter = logging.Formatter(fmt)

    return formatter


def setup_logger(path, log_level, color=True):
    from config import CFG

    formatter = get_formatter(color=False)
    color_formatter = get_formatter(color=True)

    if color:
        _set_basic_config(log_level, color_formatter)
    else:
        _set_basic_config(log_level, formatter)

    log.setLevel(logging.DEBUG)

    logfile_fname = os.path.basename(path).rstrip('.py') + ".log"
    logfile_path = os.path.join(CFG.tools_path, "log", logfile_fname)
    add_file_rotating_handler(
        logfile_path, log_level=logging.DEBUG, color=True, verbose=True)

    log.debug("  CALL  ".center(80, '#'))
    with suppress(OSError):
        log.debug("cwd: {cwd}".format(cwd=os.getcwd()))
    log.debug("sys.args: {args}".format(args=sys.argv))
    log.debug(DELIMETER)


def add_file_rotating_handler(path,
                              log_level=logging.INFO,
                              color=False,
                              verbose=False):
    from logging.handlers import RotatingFileHandler
    from utils.system import ensure_dir_exists_for_file

    ensure_dir_exists_for_file(path)

    file_handler = RotatingFileHandler(
        path, maxBytes=2 * 1024 * 1024, backupCount=2)
    file_handler.setLevel(log_level)
    file_handler.setFormatter(get_formatter(color, verbose))
    log.addHandler(file_handler)
