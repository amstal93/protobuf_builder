# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function

import itertools
import os
import pickle
import time
from contextlib import contextmanager
from copy import deepcopy
from functools import wraps, partial


def logged(timed=False, print_args=False):
    from utils.log import log

    def decorator(function=None):
        if function is None:
            return partial(logged, timed=timed, print_args=print_args)

        @wraps(function)
        def wrapper(*args, **kwargs):
            name = function.__name__
            if print_args:
                log.debug("call {name}({args}, {kwargs})".format(
                    name=name, args=args, kwargs=kwargs))
            else:
                log.debug("call {name}".format(name=name))

            time_start = time.time()
            result = function(*args, **kwargs)
            time_end = time.time()

            if timed:
                log.debug("%r - %2.2f sec" % (name, time_end - time_start))
            return result

        return wrapper

    return decorator


@contextmanager
def timer(label):
    from utils.log import log
    t = time.time()
    try:
        yield
    finally:
        log.info("%s: took %2.2f sec" % (label, time.time() - t))


class CachedProperty(object):
    """
    lazy property decorator

        class SomeClass(object):
            @cached_property
            def param(self):
                return computing()
    """

    def __init__(self, func):
        self.func = func

    def __get__(self, instance, cls=None):
        result = instance.__dict__[self.func.__name__] = self.func(instance)
        return result


class LazyProperty(object):
    '''
    meant to be used for lazy evaluation of an object attribute.
    property should represent non-mutable data, as it replaces itself.
    '''

    def __init__(self, fget):
        self.fget = fget
        self.func_name = fget.__name__

    def __get__(self, obj, cls):
        if obj is None:
            return None
        value = self.fget(obj)
        setattr(obj, self.func_name, value)
        return value


cached_property = CachedProperty
lazy_property = LazyProperty


def with_cache_to_file(fname):
    """
    закэшировать результат выполнения функции в файл

    @with_cache_to_file("hashes.pkl")
    def scan(root):
        hashes = defaultdict(list)
        for fname, fullpath in find_files(root):
            h = md5_for_file(fullpath)
            hashes[h].append(fullpath)
        return hashes
    """

    def decorator(function):
        @wraps(function)
        def wrapper(*args, **kwargs):
            cache_file_name = fname or "dump.pkl"
            if os.path.exists(cache_file_name):
                with open(cache_file_name) as f:
                    return pickle.load(f)

            result = function(*args, **kwargs)

            with open(cache_file_name, 'w') as f:
                pickle.dump(result, f)
            return result

        return wrapper

    return decorator


@contextmanager
def suppress(*exceptions):
    """
        ignore raised exceptions
    :param exceptions: list of Exception subclasses

        with suppress(IOError):
            open("bad_fname")
        print("still ok")
    """
    try:
        yield
    except exceptions:
        pass


def print_error(error, width=61):
    from utils.log import log
    line = "+=" * (width / 2) + '+'
    fmt = "|{error:^%d}|" % (width - 2)

    log.critical(line)
    log.critical(fmt.format(error=error))
    log.critical(line)


def exit_with_error(error):
    print_error(error)
    exit(1)


def ask_yes_no(message, default='y'):
    from .cli import is_assume_yes
    if is_assume_yes():
        return True

    choices = 'Y/n' if default.lower() in ('y', 'yes') else 'y/N'
    choice = raw_input("%s (%s): " % (message, choices))
    values = ('y', 'yes', '') if default == 'y' else ('y', 'yes')
    return choice.strip().lower() in values


def human_size(nbytes):
    _suffixes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB']
    if nbytes == 0:
        return '0 B'
    i = 0
    while nbytes >= 1024 and i < len(_suffixes) - 1:
        nbytes /= 1024.
        i += 1
    f = ('%.2f' % nbytes).rstrip('0').rstrip('.')
    return '%s %s' % (f, _suffixes[i])


def dict_merge(a, b):
    """
    recursively merges dict's. not just simple a['key'] = b['key'], if
    both a and bhave a key who's value is a dict then dict_merge is called
    on both values and the result stored in the returned dictionary.
    :param a: dict
    :param b: dict
    """
    if not isinstance(b, dict):
        return b

    result = deepcopy(a)

    for k, v in b.items():
        if k in result and isinstance(result[k], dict):
            result[k] = dict_merge(result[k], v)
        else:
            result[k] = deepcopy(v)
    return result


def iter_lines(fname, n):
    with open(fname) as f:
        lines = f.readlines()

        i = iter(lines)
        piece = list(itertools.islice(i, n))
        while piece:
            yield '\n'.join(piece)
            piece = list(itertools.islice(i, n))


class Enum(object):
    @classmethod
    def all(cls):
        obj = cls()
        return [getattr(obj, attr)
                for attr in dir(obj)
                if not callable(getattr(obj, attr)) and \
                not attr.startswith("__")]


def now():
    '2009_01_05_22_14_39'
    from time import gmtime, strftime
    return strftime("%Y_%m_%d_%H_%M_%S", gmtime())
