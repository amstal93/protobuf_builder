from __future__ import print_function

import contextlib
import getpass
import os
from datetime import datetime
import time

from .errors import RunningLocked
from .fabric_wrap import is_remote_path_exists, read_file, remove_file, \
    write_to_file
from .log import log


def get_pid():
    # todo implement correct get_pid()
    # from subprocess import check_output
    # return check_output(["pidof", name])
    return os.getpid()


def _get_lockfile_fullpath(path, function_name):
    return "{path}.{function}.lock".format(
        path=os.path.abspath(path), function=function_name)


def with_lockfile(path, function, created_by=None):
    """
    create lockfile for function

    @with_lockfile(__file__)
    def some_function(*args, **kwargs):
        print("synchronized")
        time.sleep(100500)


    $ python tools/lockfile.py
    $ cat lockfile.py.test_function.lock
        0 | 2016-01-28 20:28:05 | vtaranov
    """
    from utils.log import log

    def decorator(func):
        def wrapped(*args, **kwargs):

            lockfile_path = _get_lockfile_fullpath(
                path, function_name=function)

            if os.path.exists(lockfile_path):
                with open(lockfile_path) as f:
                    msg = "lockfile '{path}' exists: {data}".format(
                        path=lockfile_path, data=f.read())
                    log.critical(msg)
                    exit(1)

            try:
                file_data = "{pid} | {date} | {user} | {created_by}".format(
                    pid=get_pid(),
                    date=datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                    user=getpass.getuser(),
                    created_by=created_by or '')
                with open(lockfile_path, 'w') as out:
                    log.debug("create lockfile {}".format(lockfile_path))
                    out.write(file_data)

                return func(*args, **kwargs)
            finally:
                os.remove(lockfile_path)

        return wrapped

    return decorator


@contextlib.contextmanager
def remote_lockfile(file_path):
    """
    create lock file on remote host, fabric init required
    env.user = _asset_config["login"]
    env.host_string = env.user + "@" + _asset_config["server"]
    with remote_lockfile('/path/to/file'):
        run('''echo "hello world" ''')
    """

    log.debug('check remote lockfile {}'.format(file_path))
    if is_remote_path_exists(file_path):
        log.debug('remote lockfile {} exists'.format(file_path))
        user = read_file(file_path)

        msg = "Assets_manager is already running by {user}. file={path}".format(
            user=user, path=file_path)
        raise RunningLocked(msg)

    log.debug('creating remote lock_file {}'.format(file_path))

    write_to_file(file_path, data=getpass.getuser())

    try:
        yield
    finally:
        if is_remote_path_exists(file_path):
            remove_file(file_path)


def synchronized(lock):
    """ Synchronization decorator. """

    def wrap(f):
        def new_function(*args, **kw):
            with lock:
                return f(*args, **kw)

        return new_function

    return wrap


@contextlib.contextmanager
def wait_lockfile(path, function, max_duration=600, step=10):
    lockfile_path = _get_lockfile_fullpath(path, function_name=function)

    for waited in range(0, max_duration, step):
        if not os.path.exists(lockfile_path):
            yield
            return
        log.info("waiting lockfile '{}': {} sec".format(lockfile_path, waited))
        time.sleep(step)
    raise RunningLocked(path)
